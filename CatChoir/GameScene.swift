//
//  GameScene.swift
//  CatChoir
//
//  Created by Yvonne.H on 2016/12/30.
//  Copyright © 2016年 YvonneH. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    var catWhite:SKSpriteNode?
    var catRed:SKSpriteNode?
    var catYellow:SKSpriteNode?
    var catGreen:SKSpriteNode?
    var catBlue:SKSpriteNode?
    var catPurple:SKSpriteNode?
    var catBlack:SKSpriteNode?
    
    var popWhite:SKSpriteNode?
    var popRed:SKSpriteNode?
    var popYellow:SKSpriteNode?
    var popGreen:SKSpriteNode?
    var popBlue:SKSpriteNode?
    var popPurple:SKSpriteNode?
    var popBlack:SKSpriteNode?
    
    let meowDO = SKAction.playSoundFileNamed("MeowDO.mp3", waitForCompletion: true)
    let meowRE = SKAction.playSoundFileNamed("MeowRE.mp3", waitForCompletion: true)
    let meowMI = SKAction.playSoundFileNamed("MeowMI.mp3", waitForCompletion: true)
    let meowFA = SKAction.playSoundFileNamed("MeowFA.mp3", waitForCompletion: true)
    let meowSO = SKAction.playSoundFileNamed("MeowSO.mp3", waitForCompletion: true)
    let meowLA = SKAction.playSoundFileNamed("MeowLA.mp3", waitForCompletion: true)
    let meowSI = SKAction.playSoundFileNamed("MeowSI.mp3", waitForCompletion: true)

    
    override func didMove(to view: SKView) {
        catWhite = childNode(withName: "whiteCat") as? SKSpriteNode
        popWhite = childNode(withName: "popDO") as? SKSpriteNode
        catRed = childNode(withName: "redCat") as? SKSpriteNode
        popRed = childNode(withName: "popRE") as? SKSpriteNode
        catYellow = childNode(withName: "yellowCat") as? SKSpriteNode
        popYellow = childNode(withName: "popMI") as? SKSpriteNode
        catGreen = childNode(withName: "greenCat") as? SKSpriteNode
        popGreen = childNode(withName: "popFA") as? SKSpriteNode
        catBlue = childNode(withName: "blueCat") as? SKSpriteNode
        popBlue = childNode(withName: "popSO") as? SKSpriteNode
        catPurple = childNode(withName: "purpleCat") as? SKSpriteNode
        popPurple = childNode(withName: "popLA") as? SKSpriteNode
        catBlack = childNode(withName: "blackCat") as? SKSpriteNode
        popBlack = childNode(withName: "popSI") as? SKSpriteNode
        
        popWhite?.isHidden = true
        popRed?.isHidden = true
        popYellow?.isHidden = true
        popGreen?.isHidden = true
        popBlue?.isHidden = true
        popPurple?.isHidden = true
        popBlack?.isHidden = true
        }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            let touchNode = nodes(at: location)
            for node in touchNode {
                
                if node.name == "whiteCat" {

                    run(meowDO)
                    popWhite?.isHidden=false
                    popWhite?.run(popActions)
                }
                
                if node.name == "redCat" {
                    run(meowRE)
                    popRed?.isHidden=false
                    popRed?.run(popActions)
                }
                
                if node.name == "yellowCat" {
                    run(meowMI)
                    popYellow?.isHidden=false
                    popYellow?.run(popActions)
                }
                
                if node.name == "greenCat" {
                    run(meowFA)
                    popGreen?.isHidden=false
                    popGreen?.run(popActions)
                }
                
                if node.name == "blueCat" {
                    run(meowSO)
                    popBlue?.isHidden=false
                    popBlue?.run(popActions)
                }
                
                if node.name == "purpleCat" {
                    run(meowLA)
                    popPurple?.isHidden=false
                    popPurple?.run(popActions)
                }
                
                if node.name == "blackCat" {
                    run(meowSI)
                    popBlack?.isHidden=false
                    popBlack?.run(popActions)
                }
            }}

    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for touch in touches {
            let location = touch.location(in: self)
            let touchNode = nodes(at: location)
            for node in touchNode {
                    
            if node.name == "whiteCat" {
                run(meowDO)
                popWhite?.isHidden=false
                popWhite?.run(popActions)
            }
                
            if node.name == "redCat" {
                run(meowRE)
                popRed?.isHidden=false
                popRed?.run(popActions)
            }
                
            if node.name == "yellowCat" {
                run(meowMI)
                popYellow?.isHidden=false
                popYellow?.run(popActions)
            }
            
            if node.name == "greenCat" {
                run(meowFA)
                popGreen?.isHidden=false
                popGreen?.run(popActions)
            }
                
            if node.name == "blueCat" {
                run(meowSO)
                popBlue?.isHidden=false
                popBlue?.run(popActions)
            }
            
            if node.name == "purpleCat" {
                run(meowLA)
                popPurple?.isHidden=false
                popPurple?.run(popActions)
            }
                
            if node.name == "blackCat" {
                run(meowSI)
                popBlack?.isHidden=false
                popBlack?.run(popActions)
            }
                
        }}}
}

let popFadeIn = SKAction.fadeAlpha(to: 1.0, duration: 0.4)
let popScaleUP = SKAction.scale(to: 1.3, duration: 0.3)
let popActionsIn = SKAction.group([popFadeIn, popScaleUP])
let popFadeOut = SKAction.fadeAlpha(to: 0.0, duration: 0.2)
let popScaleDown = SKAction.scale(to: 1, duration: 0.3)
let popActionsOut = SKAction.group([popFadeOut, popScaleDown])
let popActions = SKAction.sequence([popActionsIn,popActionsOut])

